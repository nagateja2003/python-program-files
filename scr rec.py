from PIL import ImageGrab
import numpy as np
import cv2
from win32api import GetSystemMetrics
import datetime

width = GetSystemMetrics(0)
height = GetSystemMetrics(1)

time_stamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M %p')
name = input("Please enter the name to save the mp4 file : ")

fourcc = cv2.VideoWriter_fourcc(* 'XVID')
captured_video = cv2.VideoWriter(name+".mp4", fourcc, 20.0, (width,height))
print("Recording.....")

while True:
    img = ImageGrab.grab(bbox=(0, 0, width, height))
    img_np = np.array(img)
    img_final = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
    cv2.imshow("Screen Recorder",img_final)
    captured_video.write(img_final)

    if(cv2.waitKey (1)&0xFF == ord ('q')):
        captured_video.release()
        cv2.destroyAllWindows()
        break




    
