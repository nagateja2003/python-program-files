import cv2
import cvzone
from cvzone.SelfiSegmentationModule import SelfiSegmentation
import os


cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)
segementor = SelfiSegmentation()

fpsReader = cvzone.FPS()

ListImg = os.listdir("/BG Images")
imgList = []
for imgPath in ListImg:
    img = cv2.imread(f'BG Images/{imgPath}')
    imgList.append(img)

index=0
print(ListImg, len(imgList))

while True:
    sucess , img = cap.read()
    imgOut = segementor.removeBG(img, imgList[index], threshold=0.8)

    imgStacked = cvzone.stackImages([img, imgOut],2,1)
    fps, imgStacked = fpsReader.update(imgStacked, color=(0,0,255))
    
    
    cv2.putText(imgStacked, "BG-"+str(index+1), (600, 50), cv2.FONT_HERSHEY_COMPLEX, 1, (0,0,255), 2)
    cv2.imshow("With & Without BackGround", imgStacked)
    key = cv2.waitKey(1)
    if(key == ord('a')):
        if(index > 0):
            index -= 1
    elif(key == ord('d')):
        if(index < len(imgList)-1):
            index +=1
    elif(key == ord('q')):
        break

    
    
    



